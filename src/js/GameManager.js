import paper from 'paper';
import { ACTOR_STATE, Timer } from './Game';
import { SpawnManager } from './SpawnManager';
import { MINE_TYPE, MINE_SIZE } from './MineBase';
import { Ship } from './Ship';

const GAME_STATE = {
  IDLE: 1,
  OVER: 2,
  STARTING: 3,
  PLAYING: 4,
  ENDING: 5,
  INIT: 6,
};

function timer(ms) {
  return new Promise((res) => setTimeout(res, ms));
}

export class GameManager {
  constructor() {
    console.log('GameManager constructor');
    this.state = GAME_STATE.INIT;
    this.score = 0;
    this.firstExtraShip = 10000;
    this.extraShipInterval = 20000;
    this.timer = new Timer();
    this.ship = null;
    this.bulletManagers = [];
    this.spawnManager = new SpawnManager();
    this.ship = new Ship(paper.view.center);
  }

  initGame() {
    console.log('GameManager.initGame', this.timer.getTimestamp());
    this.spawnManager.setupSpawners();
    this.mines = this.spawnManager.spawn(MINE_SIZE.LG, 4);
  }

  async startGame() {
    console.log('GameManager.startGame', this.timer.getTimestamp());
    this.state = GAME_STATE.PLAYING;
    console.log(this.mines);
    for (let mine of this.mines) {
      await timer(800);
      mine.activate();
    }
  }

  update() {
    if (this.state === GAME_STATE.PLAYING) {
      // update ship
      this.ship.update();
      // move mines
      for (let mine of this.mines) {
        mine.update();
      }
    }
  }
}
