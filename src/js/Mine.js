import paper from 'paper';
import { MineBase, MINE_SIZE } from './MineBase';
import { swapColor } from './Game';
import { BulletManager } from './BulletManager';

export class Mine extends MineBase {
  constructor(size, position) {
    console.log('Mine constructor');
    super();
    this.size = size || MINE_SIZE.LG;
    this.angle = Math.floor(Math.random() * Math.floor(359));
    this.vector = new paper.Point({ angle: this.angle, length: 1 });
    this.item = this.build();
    this.item.visible = false;
    this.item.position = position;
    this.bulletManager = new BulletManager();
  }

  build() {
    console.log('Mine build');
    swapColor();
    let body = new paper.Path(
      [0, 25],
      [5, 5],
      [25, 0],
      [5, -5],
      [0, -25],
      [-5, -5],
      [-25, 0],
      [-5, 5]
    );
    body.closed = true;
    swapColor('red');
    let charge = new paper.Path([10, 10], [10, -10], [-10, -10], [-10, 10]);
    charge.closed = true;
    let group = new paper.Group(body, charge);
    // scale based on size
    group.scale(this.size);
    swapColor();
    return group;
  }
}
