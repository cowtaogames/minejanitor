import paper from 'paper';
import { swapColor } from './Game';
import { MINE_SIZE, MINE_TYPE } from './MineBase';
import { Mine } from './Mine';

export const SPAWN_STATE = {
  IDLE: 1,
  SPAWNING: 2,
  INACTIVE: 3,
};

export class Spawner {
  constructor(pos) {
    this._state = SPAWN_STATE.IDLE;
    this._item = this.build();
    this._item.position = pos;
  }

  build() {
    swapColor();
    let p1 = new paper.Path([1, 1], [-1, -1]);
    let p2 = new paper.Path([-1, 1], [1, -1]);
    return new paper.Group(p1, p2);
  }

  spawn(type, size) {
    this._state = SPAWN_STATE.SPAWNING;
    this._item.visible = false;
    // TODO:  spawn based on Mine Type
    let mine = new Mine(size, this._item.position);
    this._state = SPAWN_STATE.INACTIVE;
    return mine;
  }

  get item() {
    return this._item;
  }

  get pos() {
    return this._item.position;
  }

  set pos(newPos) {
    this._item.position = newPos;
  }
}
