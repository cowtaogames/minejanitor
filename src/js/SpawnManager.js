import paper from 'paper';
import { Mine } from './Mine';
import { MINE_TYPE } from './MineBase';
import { Spawner, SPAWN_STATE } from './Spawn';

export class SpawnManager {
  constructor() {
    this.roundNumber = 1;
    this._spawners = [];
    this.setBoundaries();
  }

  setBoundaries() {
    // set some boundaries, so as to not spawn
    //   within 10% of the view size from the edges
    let viewX = paper.view.size.width;
    let viewY = paper.view.size.height;
    this.maxX = viewX - 0.1 * viewX;
    this.maxY = viewY - 0.1 * viewY;
    this.minX = 0.1 * viewX;
    this.minY = 0.1 * viewY;
  }

  setupSpawners() {
    console.log('setupSpawners');
    let total = 28;
    for (let l = 0; l < total; l++) {
      let posX = Math.floor(
        Math.random() * (this.maxX - this.minX + 1) + this.minX
      );
      let posY = Math.floor(
        Math.random() * (this.maxY - this.minY + 1) + this.minY
      );
      let spawner = new Spawner(new paper.Point(posX, posY));
      this._spawners.push(spawner);
    }
  }

  spawn(size, count = 2) {
    console.log('spawn', size, count);
    let spawned = [];
    for (let i = 0; i < count; i++) {
      let spawner = this._spawners.pop();
      spawned.push(spawner.spawn(null, size));
    }
    return spawned;
  }
}
