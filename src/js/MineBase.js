import paper from 'paper';
import { ACTOR_STATE, Timer } from './Game';

export const MINE_TYPE = {
  DRIFTER: 1,
  SHOOTER: 2,
  CHASER: 3,
  CHASER_SHOOTER: 4,
};

export const MINE_SIZE = {
  SM: 0.4,
  MD: 0.7,
  LG: 1.2,
};

export class MineBase {
  constructor() {
    this.item = null;
    this.position = null;
    this.angle = 0;
    this.vector = new paper.Point({ angle: this.angle, length: 1 });
    this.speed = 0.15;
    this.rotation = Math.random() * 0.6 * (Math.round(Math.random()) ? 1 : -1);
    this.bulletManager = null;
    this.state = ACTOR_STATE.INACTIVE;
    this.timer = new Timer();
    this.lastUpdate = this.timer.getTimestamp();
  }

  activate() {
    console.log('MineBase activate', this.timer.getTimestamp());
    this.item.visible = true;
    this.state = ACTOR_STATE.MOVING;
  }

  update() {
    if (this.state === ACTOR_STATE.MOVING) {
      this.item.position.x += this.vector.x;
      this.item.position.y += this.vector.y;
      this.item.rotate(this.rotation);
      this.keepInView();
    }
  }

  stop() {
    this.vector.length = 0;
  }

  start() {
    this.vector.length = 1;
  }

  keepInView() {
    let position = this.item.position;
    let itemBounds = this.item.bounds;
    let bounds = paper.view.bounds;

    if (itemBounds.left > bounds.width) {
      position.x = -this.item.bounds.width;
    }

    if (position.x < -itemBounds.width) {
      position.x = bounds.width;
    }

    if (itemBounds.top > paper.view.size.height) {
      position.y = -itemBounds.height;
    }

    if (position.y < -itemBounds.height) {
      position.y = bounds.height + itemBounds.height / 2;
    }
  }
}
