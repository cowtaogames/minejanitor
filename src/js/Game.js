import paper from 'paper';

export const MAIN_COLOR = new paper.Color(0.85, 0.996, 1, 0.8);

export const ACTOR_STATE = {
  SPAWNING: 1,
  MOVING: 2,
  DYING: 3,
  DEAD: 4,
  INACTIVE: 5,
};

export const swapColor = function (color) {
  if (color) {
    paper.project.currentStyle.strokeColor = color;
  } else {
    paper.project.currentStyle.strokeColor = MAIN_COLOR;
  }
};

export const keepInView = function (item) {
  if (!item) {
    return;
  }
  let position = item.position;
  let itemBounds = item.bounds;
  let bounds = paper.view.bounds;

  if (itemBounds.left > bounds.width) {
    position.x = -item.bounds.width;
  }

  if (position.x < -itemBounds.width) {
    position.x = bounds.width;
  }

  if (itemBounds.top > paper.view.size.height) {
    position.y = -itemBounds.height;
  }

  if (position.y < -itemBounds.height) {
    position.y = bounds.height + itemBounds.height / 2;
  }
};

export class Timer {
  constructor() {
    if (window.performance.now) {
      console.log('Using high performance timer');
      this.getTimestamp = function () {
        return window.performance.now();
      };
    } else {
      if (window.performance.webkitNow) {
        console.log('Using webkit high performance timer');
        this.getTimestamp = function () {
          return window.performance.webkitNow();
        };
      } else {
        console.log('Using low performance timer');
        this.getTimestamp = function () {
          return new Date().getTime();
        };
      }
    }
  }
}
