import paper from 'paper';
import { swapColor } from './Game';
import { BulletManager } from './BulletManager';

export class Ship {
  constructor(position = paper.view.bounds.center) {
    this._item = this.build();
    this._item.position = position || paper.view.bounds.center;
    this._item.pivot = this._item.bounds.center;
    this._angle = 0;
    this._vector = new paper.Point({
      angle: 0.2,
      length: 1,
    });
    this._speed = 0.2;
    this._bulletManager = new BulletManager();
  }

  // movement
  turnLeft() {
    this._item.rotate(-3);
    this._angle -= 3;
  }

  turnRight() {
    this._item.rotate(3);
    this._angle += 3;
  }

  thrust() {
    this._thrust.visible = true;
    let delta = new paper.Point({
      angle: this._angle,
      length: this._speed,
    });
    this._vector.x += delta.x;
    this._vector.y += delta.y;
    if (this._vector.length > 8) {
      this._vector.length = 8;
    }
  }

  coast() {
    this._thrust.visible = false;
    this._vector.length *= 0.9;
  }

  update() {
    this._item.position.x += this._vector.x;
    this._item.position.y += this._vector.y;
    this.keepInView();
    this._bulletManager.update();
  }

  build() {
    let body = new paper.Path([10, 0], [-8, -5], [-8, 5]);
    body.closed = true;
    let wings = new paper.Path(
      [-2, 8],
      [-8, 6],
      [-8, -6],
      [-2, -8],
      [-10, -8],
      [-8, 0],
      [-10, 8]
    );
    wings.closed = true;
    swapColor('#ff8b26');
    let thrust = new paper.Path([-9, -2], [-15, 0], [-9, 2]);
    // set this so it can be toggled on and off
    this._thrust = thrust;
    swapColor();
    return new paper.Group(body, wings, thrust);
  }

  keepInView() {
    let position = this._item.position;
    let itemBounds = this._item.bounds;
    let bounds = paper.view.bounds;

    if (itemBounds.left > bounds.width) {
      position.x = -this._item.bounds.width;
    }

    if (position.x < -itemBounds.width) {
      position.x = bounds.width;
    }

    if (itemBounds.top > paper.view.size.height) {
      position.y = -itemBounds.height;
    }

    if (position.y < -itemBounds.height) {
      position.y = bounds.height + itemBounds.height / 2;
    }
  }

  fire() {
    this._bulletManager.shoot(this._item.position, this._angle);
  }

  get item() {
    return this._item;
  }

  get bulletManager() {
    return this._bulletManager;
  }

  get angle() {
    return this._angle;
  }

  set angle(newAngle) {
    this._angle = newAngle;
  }

  get speed() {
    return this._speed;
  }

  set speed(newSpeed) {
    this._speed = newSpeed;
  }

  get vector() {
    return this._vector;
  }

  set vector(newVector) {
    this._vector = newVector;
  }
}
