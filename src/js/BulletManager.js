import paper from 'paper';
import { keepInView } from './Game';

export class BulletManager {
  constructor() {
    this._bullets = new paper.Group();
    this._children = this._bullets.children;
    this._ttl = 60;
    this._max = 5;
  }

  shoot(pos, angle) {
    // 5 bullets max at any given time
    if (this._children.length == this._max) {
      return;
    }
    let vector = new paper.Point({ angle, length: 9 });
    let center = new paper.Point(pos.x, pos.y);
    center.x += vector.length;
    center.y += vector.length;
    let bullet = new paper.Path.Circle({
      center,
      radius: 0.5,
      // parent: this._bullets,
      fillColor: '#ffffff',
      strokeWidth: 0,
      data: {
        vector,
        timeToDie: 58,
      },
    });
    this._bullets.addChild(bullet);
  }

  update() {
    for (let bullet of this._bullets.children) {
      bullet.data.timeToDie--;
      if (bullet.data.timeToDie < 1) {
        bullet.remove();
      } else {
        bullet.position.x += bullet.data.vector.x;
        bullet.position.y += bullet.data.vector.y;
        keepInView(bullet);
      }
    }
  }

  get ttl() {
    return this._ttl;
  }

  set ttl(newTtl) {
    this._ttl = newTtl;
  }

  get max() {
    return this._max;
  }

  set max(newMax) {
    this._max = newMax;
  }
}
