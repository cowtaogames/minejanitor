import paper from 'paper';
import { Ship } from './Ship';
import { SpawnManager } from './SpawnManager';
import { Timer } from './Game';
import { GameManager } from './GameManager';

import '../css/papermines.css';
import { MINE_SIZE, MINE_TYPE } from './MineBase';

window.onload = function () {
  const canvas = document.getElementById('canvas');
  paper.setup(canvas);
  paper.project.currentStyle.strokeColor = new paper.Color(0.85, 0.996, 1, 0.8);

  // load GM
  const gameManager = new GameManager();

  // setup event handlers
  // paper's onFrame handler
  paper.view.onFrame = function () {
    gameManager.update();
    if (paper.Key.isDown('left')) {
      gameManager.ship.turnLeft();
    }
    if (paper.Key.isDown('right')) {
      gameManager.ship.turnRight();
    }
    if (paper.Key.isDown('up')) {
      gameManager.ship.thrust();
    } else {
      gameManager.ship.coast();
    }
  };

  // INPUT handling
  document.onkeyup = function (event) {
    if (event.key == 'z') {
      gameManager.ship.fire();
    }
  };

  // Stop left and right keyboard events from propagating.
  document.onkeydown = function (event) {
    if (event.key == 'left' || event.key == 'right') {
      return false;
    }
  };

  gameManager.initGame();
  gameManager.startGame();
};
